## Function from which I want
## to sample. Support is 0 to 1.
f <- function(x) {
    x^(3) * (1 - x)^(10)
}

x <- seq(0, 1, length.out=1e3)
fxv <- f(x)

## Uniform with support from 0 to 1. 
gxv <- rep(1, 1e3)

## Constant to adjust the height so min of M*uniform density
## corresponds to max of f density. 
M <- 1/min(gxv/fxv)

propose <- function(f, M, niter=1e3) {
    ## Make a proposal between 0 and 1.
    ## Accept values in proportion of their densities
    ## on f.
    ## Return the chain of accepted values.
    chain <- rep(NA, niter)

    for(i in 1:niter) {
        prop <- runif(1)
        if(runif(1) <= f(prop)/M) {
            chain[i] <- prop
        }
    }
    chain <- chain[!is.na(chain)]

    return(chain)
}

hist(propose(f, M, 1e5), prob=TRUE)
curve(dbeta(x, 4, 11), add=TRUE)


