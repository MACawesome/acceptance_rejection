## Probability of water
psides <- c(0.1, 0.9)
## Markov chain
side <- seq(1, 2)
niter <- 1e4
chain <- rep(NA, niter)
current <- 1
chain[1] <- current

for(i in 2:niter) {
    proposal <- side[-current]
    prop.freq <- psides[proposal]
    current.freq <- psides[current]
    threshold <- min(prop.freq/current.freq, 1)
    if(runif(1) < threshold) {
        current <- proposal
        chain[i] <-  current
    }
    else {
        chain[i] <- current
    }
}

hist(chain, prob=TRUE)
    
    

