rm(list=ls())
library(rethinking)

sppnames <- c( "afarensis","africanus","habilis","boisei",
              "rudolfensis","ergaster","sapiens")
brainvolcc <- c( 438 , 452 , 612, 521, 752, 871, 1350 )
masskg <- c( 37.0 , 35.5 , 34.5 , 41.5 , 55.5 , 61.0 , 53.5 )
d <- data.frame( species=sppnames , brain=brainvolcc , mass=masskg )

str(d)

d$mass.s <- with(d, (mass - mean(mass))/sd(mass))

m6.8 <- map(
    alist(
        brain ~ dnorm(mu, sigma),
        mu <- a + b*mass.s),
    data=d,
    start=list(a=mean(d$brain), b=0, sigma=sd(d$brain))
)

theta <- coef(m6.8)

theta


