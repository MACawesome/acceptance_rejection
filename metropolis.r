## Simulates the travels of King Markov on
## 10 islands of diverse populations.
## King Markov can only choose one of two neighboring islands
## to travel next. 
## The goal is to spend an amount of time in each
## island proportional to its population.

get_neighbors <- function(start, islands) {
    if(start == min(islands)) {
        return(c(max(islands), start + 1))
    }
    else if (start == max(islands)) {
        return(c(start - 1, min(islands)))
    }
    else {
        return(c(start -1, start + 1))
    }
}

pops <- round(runif(10, 1, 100))
islands <- seq(1, length(pops))

markov <- function(islands, pops, niter=1e3) {
    ## Visit islands depending on their relative amount
    ## of inhabitants
    ## Return visits to each island. 
    start <- sample(islands, 1)
    current <- start

    chain <- rep(NA, niter)
    chain[1] <- current

    for(i in 2:niter) {
        ## Make a proposal for new island to visit
        neighbors <- get_neighbors(current, islands)
        u <- runif(1)
        proposal <- round(runif(1))

        if(proposal == 0) {
            ## This and the next conditional decides
            ## whether King Markov stays or goes. 
            p <- pops[neighbors[1]]/pops[current]
            threshold <- min(p, 1)
            if(u < threshold) {
                current <- neighbors[1]
            }
            else {
                current <- current
            }
        }
        if(proposal == 1) {
            p <- pops[neighbors[2]]/pops[current]
            threshold <- min(p, 1)
            if(u < threshold) {
                current <- neighbors[2]
            }
            else {
                current <- current
            }
        }
        chain[i] <- current
    }
    return(chain)
}

tmp.chain <- markov(islands, pops, niter=1e4)
plot(tmp.chain, type="l")
hist(tmp.chain)
which.max(pops)


